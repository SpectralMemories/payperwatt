package org.spectralmemories.payperwatt;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.AnaloguePowerable;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import javax.swing.*;

//Handles the right clicking of a sell panel
final public class SignRightClickListener implements Listener
{
    public static final String USE_PERMISSION = "payperwatt.use";
    public static final String NOT_ENOUGH_MONEY_ERROR = "Sorry, you do not have enough money for that";
    public static final String NO_REDSTONE_FOUND_ERROR = "No redstone was found to activate. You were refunded";
    public static final String SUCCESSFULLY_PAID = "Successfully paid ";
    public static final String CURRENCY = "$ to ";



    public SignRightClickListener ()
    {

    }

    @EventHandler
    public void onBlockRightClicked (PlayerInteractEvent event)
    {
        try
        {
            if (event == null) return;
            if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

            if (event.getClickedBlock().getState() instanceof Sign) {

                final Sign clickedSign = (Sign) event.getClickedBlock().getState();
                String[] lines = clickedSign.getLines();

                if (lines[0].equalsIgnoreCase(PayPerWatt.SIGN_HEADER)) {

                    final Player clickingPlayer = event.getPlayer();

                    if (!clickingPlayer.hasPermission(USE_PERMISSION)) {
                        clickingPlayer.sendMessage(ChatColor.DARK_RED + PayPerWatt.NO_PERMISSION_MESSAGE);
                        return;
                    }

                    String signOwner = lines[1];
                    if (signOwner.isEmpty()) {
                        //Somehow this sign is borked
                        return;
                    }

                    if (signOwner.equals(clickingPlayer.getDisplayName()))
                    {
                        //Own sign
                        ActivateRedstoneAround(clickedSign.getLocation());
                    }
                    else
                    {
                        //Other's sign
                        final Economy economy = PayPerWatt.economy;

                        int cost = 0;
                        try {
                            cost = Integer.parseInt(lines[2]);
                        } catch (Exception e) {
                            //Lets remain silent for now
                            //Bukkit.broadcastMessage("Error while parsing PayPerWatt sign: not a number!");
                            return;
                        }


                        EconomyResponse result = economy.withdrawPlayer(clickingPlayer, cost);

                        if (result.type != EconomyResponse.ResponseType.SUCCESS) {
                            clickingPlayer.sendMessage(ChatColor.GOLD + NOT_ENOUGH_MONEY_ERROR);
                            return;
                        }


                        if (!ActivateRedstoneAround(clickedSign.getLocation())) {
                            clickingPlayer.sendMessage(ChatColor.DARK_RED + NO_REDSTONE_FOUND_ERROR);
                            economy.depositPlayer(clickingPlayer, cost);
                            return;
                        }

                        Player signOwnerPlayer = Bukkit.getPlayer(signOwner);
                        if (signOwnerPlayer != null)
                        {
                            //Player is online
                            economy.depositPlayer(signOwnerPlayer, cost);
                            signOwnerPlayer.sendMessage(ChatColor.GOLD + clickingPlayer.getDisplayName() + ChatColor.DARK_GREEN + " Has puchased from your PayPerWatt sign for " + cost + "$!");
                        }
                        else
                        {
                            //Player is offline
                            SQLAccess.GetInstance().SavePlayerDebt(new Debt(cost, signOwner, clickingPlayer.getDisplayName()));
                        }

                        clickingPlayer.sendMessage(ChatColor.DARK_GREEN + SUCCESSFULLY_PAID + cost + CURRENCY + ChatColor.GOLD + signOwner);
                    }
                }
            }
        }
        catch (Exception e){}
    }

    private boolean ActivateRedstoneAround (Location location)
    {
        boolean foundRedstone = false;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    Location checkLocation = location.clone();
                    checkLocation.add (x, y, z);

                    Block realBlock = checkLocation.getBlock();
                    BlockData blockData = realBlock.getBlockData();


                    if (blockData instanceof AnaloguePowerable)
                    {
                        AnaloguePowerable powerable = (AnaloguePowerable) blockData;
                        powerable.setPower(15);

                        PayPerWatt.GetInstance().getServer().getScheduler().runTaskLater(PayPerWatt.GetInstance(), new KeepRedstoneOn(realBlock, 30), 1);
                        foundRedstone = true;
                    }
                }
            }
        }
        return foundRedstone;
    }

    private final class KeepRedstoneOn implements Runnable
    {
        private Block blockToPower;
        private int remainingTicks;

        public KeepRedstoneOn(Block blockToPower, int remainingTicks)
        {
            this.blockToPower = blockToPower;
            this.remainingTicks = remainingTicks;
        }

        @Override
        public void run()
        {
            AnaloguePowerable powerable = (AnaloguePowerable) blockToPower.getBlockData();
            powerable.setPower(15);
            blockToPower.setBlockData(powerable);

            if (--remainingTicks > 0)
            {
                PayPerWatt plugin = PayPerWatt.GetInstance();
                plugin.getServer().getScheduler().runTaskLater(plugin, this, 1);
            }
        }
    }
}

