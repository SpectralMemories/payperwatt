package org.spectralmemories.payperwatt;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;


public final class PayPerWatt extends JavaPlugin
{

    private static PayPerWatt instance;

    public static final String SIGN_HEADER = "[PPW]";
    public static final String NO_PERMISSION_MESSAGE = "You do not have permission to do that";

    public static final String COULD_NOT_LOAD_VAULT = "Error! Could not load Vault";
    public static Economy economy;

    public PayPerWatt ()
    {
        instance = this;
    }

    public static PayPerWatt GetInstance ()
    {
        return instance;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic

        if (! setupEconomy())
        {
            //Bukkit.broadcastMessage(COULD_NOT_LOAD_VAULT);
            //return;
            getServer().shutdown();
        }

        getServer().getPluginManager().registerEvents(new SignRightClickListener(), this);
        getServer().getPluginManager().registerEvents(new SignPlaceListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        SQLAccess.GetInstance().Close();
    }

    private final boolean setupEconomy ()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null)
        {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }
}
