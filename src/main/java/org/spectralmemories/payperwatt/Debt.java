package org.spectralmemories.payperwatt;

public class Debt
{
    int amount;
    String to;
    String from;


    public Debt (int amount, String to, String from)
    {
        this.amount = amount;
        this.to = to;
        this.from = from;
    }
}
