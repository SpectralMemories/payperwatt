package org.spectralmemories.payperwatt;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;

public final class PlayerJoinListener implements Listener
{
    @EventHandler
    public void onPlayerConnect (PlayerJoinEvent event)
    {
        SQLAccess dbAccess = SQLAccess.GetInstance();

        Player player = event.getPlayer();
        String enteringPlayerName = player.getDisplayName();

        ArrayList<Debt> debts = dbAccess.WithdrawPlayerDebts(enteringPlayerName);

        for (int i = 0; i < debts.size(); i ++)
        {
            player.sendMessage(ChatColor.GOLD + debts.get(i).from + ChatColor.DARK_GREEN + " has purchased a PayPerWatt from you for " + debts.get(i).amount + "$ While you were absent!");

            PayPerWatt.economy.depositPlayer(player, debts.get(i).amount);
        }
    }
}
