package org.spectralmemories.payperwatt;

import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public final class SQLAccess
{
    public static final String JDBC_SQLITE = "jdbc:sqlite:";
    public static final String SLASH = "/";
    private static SQLAccess instance;

    String dataFolder;
    final String DBNAME = "transactions.db";
    Connection database;

    public static SQLAccess GetInstance ()
    {
        if (instance == null)
        {
            instance = new SQLAccess();
        }
        return instance;
    }

    private SQLAccess ()
    {
        Initialize();
    }

    private void Initialize ()
    {
        dataFolder = PayPerWatt.GetInstance().getDataFolder().getAbsolutePath();
        if (! DBExists())
        {
            CreateDatabaseFile();
        }

        try
        {
            database = DriverManager.getConnection(JDBC_SQLITE + dataFolder + SLASH + DBNAME);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private boolean DBExists ()
    {
        File db = new File(dataFolder + SLASH + DBNAME);
        return db.exists();
    }

    private void CreateDatabaseFile ()
    {
        File folder = PayPerWatt.GetInstance().getDataFolder().getAbsoluteFile();
        folder.mkdir();

        try
        {
            database = DriverManager.getConnection(JDBC_SQLITE + dataFolder + SLASH + DBNAME);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return;
        }

        String sql = "CREATE TABLE IF NOT EXISTS debt (id INTEGER PRIMARY KEY AUTOINCREMENT, sender TEXT NOT NULL, receiver TEXT NOT NULL, amount INTEGER);";

        try
        {
            Statement stmt = database.createStatement();
            stmt.execute(sql);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public void Close ()
    {
        if (database != null)
        {
            try
            {
                database.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void SavePlayerDebt (Debt debt)
    {
        String sql = "INSERT INTO debt VALUES (NULL,'" + debt.from + "', '" + debt.to + "', " + debt.amount + ")";

        try
        {
            Statement statement = database.createStatement();
            statement.execute(sql);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    @Nullable
    public ArrayList<Debt> WithdrawPlayerDebts (String receiver)
    {

        String sql = "SELECT * FROM debt WHERE receiver = '" + receiver + "'";
        ArrayList<Debt> debts = new ArrayList<>();
        try
        {
            Statement statement = database.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);



            while (resultSet.next())
            {
                Debt debt = new Debt(resultSet.getInt("amount"), resultSet.getString("receiver"), resultSet.getString("sender"));
                debts.add(debt);
            }

            statement = database.createStatement();
            statement.execute("DELETE FROM debt WHERE receiver = '" + receiver + "'");

            return debts;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

}
