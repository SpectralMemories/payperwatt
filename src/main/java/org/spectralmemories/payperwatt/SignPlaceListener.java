package org.spectralmemories.payperwatt;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;



public final class SignPlaceListener implements Listener
{

    public static final String PAYPERWATT_PLACE = "payperwatt.place";
    public static final String PLEASE_WRITE_A_WHOLE_NUMBER_ON_LINE_3 = "Please, write a whole number on line 3";

    @EventHandler
    public void onBlockPlaced (SignChangeEvent event)
    {
        try
        {
            Block realBlock = event.getBlock();
            BlockState placedBlock = realBlock.getState();


            if (placedBlock instanceof Sign)
            {

                Sign placedSign = (Sign) placedBlock;

                if (! event.getLine(0).equals(PayPerWatt.SIGN_HEADER))
                {
                    return;
                }


                Player placingPlayer = event.getPlayer();

                if (! placingPlayer.hasPermission(PAYPERWATT_PLACE))
                {
                    placingPlayer.sendMessage(ChatColor.DARK_RED + PayPerWatt.NO_PERMISSION_MESSAGE);
                    realBlock.breakNaturally();
                    return;
                }



                event.setLine(1, placingPlayer.getDisplayName());

                try
                {
                    Integer.parseInt(event.getLine(2));
                }
                catch (NumberFormatException exception)
                {
                    realBlock.breakNaturally();
                    placingPlayer.sendMessage(ChatColor.GOLD + PLEASE_WRITE_A_WHOLE_NUMBER_ON_LINE_3);
                    return;
                }



                realBlock.setBlockData(placedSign.getBlockData());
                placingPlayer.sendMessage(ChatColor.DARK_GREEN + "PayPerWatt sign successfully placed!");
            }
        }
        catch (Exception e){}
    }
}
